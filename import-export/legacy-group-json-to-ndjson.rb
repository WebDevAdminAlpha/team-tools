#!/usr/bin/env ruby
# frozen_string_literal: true

# Converts a `group.json` file to the ndjson files/folder structure:
# tree
# |- groups
# |  |- 761/
# |     |- members.ndjson
# |     |- boards.ndjson
# |     |- badges.ndjson
# |     |- ...
# |  |- 761.json
# |  |- _all.ndjson
require "json"
require "fileutils"

ASSOCIATIONS = %w[milestones badges labels boards members epics].freeze
BASE_PATH = "tree/groups"

def process(json, parent_id: nil, given_id: nil)
  return if json.nil? || json.empty?

  id = get_id(json, given_id)
  collect_id(id)

  json["id"] = id
  json["parent_id"] = parent_id

  json = consume_associations(id, json)
  children = json.delete("children")

  write_json("tree/groups/#{id}.json", json)
  process_children(id, children)
end

def get_id(json, given_id)
  json["id"] ||
    json.dig("children", 0, "parent_id") ||
    given_id ||
    rand(500..1000)
end

def collect_id(id)
  FileUtils.mkdir_p("#{BASE_PATH}/#{id}")
  write_ndjson("#{BASE_PATH}/_all.ndjson", id)
end

def consume_associations(id, json)
  ASSOCIATIONS.each do |association|
    write_association(id, association, json.delete(association))
  end

  json
end

def process_children(id, children)
  Array(children).each.with_index(1) do |group, index|
    process(group, parent_id: id, given_id: id + index)
  end
end

def write_association(id, name, objects)
  objects.each do |object|
    write_ndjson("#{BASE_PATH}/#{id}/#{name}.ndjson", object)
  end
end

def write_json(path, json)
  File.write(path, JSON.generate(json))
end

def write_ndjson(path, json)
  File.write(path, "#{JSON.generate(json)}\n", nil, mode: "a")
end

original_file = ARGV[0]
process JSON.parse(File.read(original_file))
